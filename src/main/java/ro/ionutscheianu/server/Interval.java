package ro.ionutscheianu.server;

import ro.ionutscheianu.utils.Utils;

import java.util.Date;
import java.util.Objects;

public class Interval {

    private Date startingDate;
    private Date endingDate;

    public Interval(int startingDateMonth, int startingDateDay, int endingDateMonth, int endingDateDay) {
        startingDate = Utils.getDateFromInt(startingDateMonth, startingDateDay);
        endingDate = Utils.getDateFromInt(endingDateMonth, endingDateDay);
    }

    public Interval(Date startingDate, Date endingDate) {
        this.startingDate = startingDate;
        this.endingDate = endingDate;
    }

    @Override
    public String toString() {
        return startingDate + "-" + endingDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Interval interval = (Interval) o;
        return startingDate.equals(interval.startingDate) &&
                endingDate.equals(interval.endingDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startingDate, endingDate);
    }

    public boolean contains(Date currentDate) {
        return (currentDate.after(startingDate) || currentDate.equals(startingDate)) && (currentDate.before(endingDate) || currentDate.equals(endingDate));
    }

}
