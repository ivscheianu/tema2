package ro.ionutscheianu.server.service;

import io.grpc.stub.StreamObserver;
import ro.ionutscheianu.generated.AutumnServiceGrpc;
import ro.ionutscheianu.generated.ZodiacSignRequestOuterClass;
import ro.ionutscheianu.generated.ZodiacSignResponseOuterClass;
import ro.ionutscheianu.server.SignAnalyzer;

import java.io.IOException;

public class AutumnService extends AutumnServiceGrpc.AutumnServiceImplBase {

    private SignAnalyzer signAnalyzer;

    public AutumnService() throws IOException {
        signAnalyzer = new SignAnalyzer();
    }

    @Override
    public void determineSign(ZodiacSignRequestOuterClass.ZodiacSignRequest request, StreamObserver<ZodiacSignResponseOuterClass.ZodiacSignResponse> responseObserver) {
        System.out.println("Autumn service called");
        ZodiacSignResponseOuterClass.ZodiacSignResponse.Builder zodiacSignResponse = ZodiacSignResponseOuterClass.ZodiacSignResponse.newBuilder();
        zodiacSignResponse.setZodiacSign(signAnalyzer.determineSign(request.getBirthDate()));
        responseObserver.onNext(zodiacSignResponse.build());
        responseObserver.onCompleted();
    }
}
