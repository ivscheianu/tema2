package ro.ionutscheianu.server.service;

import io.grpc.stub.StreamObserver;
import ro.ionutscheianu.generated.ServiceRequestOuterClass;
import ro.ionutscheianu.generated.ServiceResponseOuterClass;
import ro.ionutscheianu.generated.ZodiacRepositoryGrpc;
import ro.ionutscheianu.utils.Utils;

import java.time.LocalDate;
import java.time.Month;

public class ZodiacRepository extends ZodiacRepositoryGrpc.ZodiacRepositoryImplBase {

    @Override
    public void determineService(ServiceRequestOuterClass.ServiceRequest request, StreamObserver<ServiceResponseOuterClass.ServiceResponse> responseObserver) {
        System.out.println("Zodiac repository called");
        LocalDate localDate = Utils.getLocalDateFromString(request.getBirthDate());
        ServiceResponseOuterClass.ServiceResponse.Builder serviceResponseBuilder = ServiceResponseOuterClass.ServiceResponse.newBuilder();
        if (isWinter(localDate)) {
            serviceResponseBuilder.setService(ServiceResponseOuterClass.ServiceType.WINTER);
            sendResponse(responseObserver, serviceResponseBuilder);
            return;
        }
        if (isSpring(localDate)) {
            serviceResponseBuilder.setService(ServiceResponseOuterClass.ServiceType.SPRING);
            sendResponse(responseObserver, serviceResponseBuilder);
            return;
        }
        if (isSummer(localDate)) {
            serviceResponseBuilder.setService(ServiceResponseOuterClass.ServiceType.SUMMER);
            sendResponse(responseObserver, serviceResponseBuilder);
            return;
        }
        if (isAutumn(localDate)) {
            serviceResponseBuilder.setService(ServiceResponseOuterClass.ServiceType.AUTUMN);
        } else {
            serviceResponseBuilder.setService(ServiceResponseOuterClass.ServiceType.UNRECOGNIZED);
        }
        sendResponse(responseObserver, serviceResponseBuilder);
    }

    private void sendResponse(StreamObserver<ServiceResponseOuterClass.ServiceResponse> responseObserver, ServiceResponseOuterClass.ServiceResponse.Builder serviceResponseBuilder) {
        responseObserver.onNext(serviceResponseBuilder.build());
        responseObserver.onCompleted();
    }

    private boolean isAutumn(LocalDate localDate) {
        return localDate.getMonth() == Month.SEPTEMBER || localDate.getMonth() == Month.OCTOBER || localDate.getMonth() == Month.NOVEMBER;
    }

    private boolean isSummer(LocalDate localDate) {
        return localDate.getMonth() == Month.JUNE || localDate.getMonth() == Month.JULY || localDate.getMonth() == Month.AUGUST;
    }

    private boolean isWinter(LocalDate localDate) {
        return localDate.getMonth() == Month.DECEMBER || localDate.getMonth() == Month.JANUARY || localDate.getMonth() == Month.FEBRUARY;
    }

    private boolean isSpring(LocalDate localDate) {
        return localDate.getMonth() == Month.MARCH || localDate.getMonth() == Month.APRIL || localDate.getMonth() == Month.MAY;
    }
}
