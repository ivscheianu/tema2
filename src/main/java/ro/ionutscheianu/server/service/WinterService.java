package ro.ionutscheianu.server.service;

import io.grpc.stub.StreamObserver;
import ro.ionutscheianu.generated.WinterServiceGrpc;
import ro.ionutscheianu.generated.ZodiacSignRequestOuterClass;
import ro.ionutscheianu.generated.ZodiacSignResponseOuterClass;
import ro.ionutscheianu.server.SignAnalyzer;

import java.io.IOException;

public class WinterService extends WinterServiceGrpc.WinterServiceImplBase {

    private SignAnalyzer signAnalyzer;

    public WinterService() throws IOException {
        signAnalyzer = new SignAnalyzer();
    }

    @Override
    public void determineSign(ZodiacSignRequestOuterClass.ZodiacSignRequest request, StreamObserver<ZodiacSignResponseOuterClass.ZodiacSignResponse> responseObserver) {
        System.out.println("Winter service called");
        ZodiacSignResponseOuterClass.ZodiacSignResponse.Builder zodiacSignResponse = ZodiacSignResponseOuterClass.ZodiacSignResponse.newBuilder();
        zodiacSignResponse.setZodiacSign(signAnalyzer.determineSign(request.getBirthDate()));
        responseObserver.onNext(zodiacSignResponse.build());
        responseObserver.onCompleted();
    }

}
