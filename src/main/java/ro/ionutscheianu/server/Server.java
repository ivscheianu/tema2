package ro.ionutscheianu.server;

import io.grpc.ServerBuilder;
import ro.ionutscheianu.server.service.AutumnService;
import ro.ionutscheianu.server.service.SpringService;
import ro.ionutscheianu.server.service.SummerService;
import ro.ionutscheianu.server.service.WinterService;
import ro.ionutscheianu.server.service.ZodiacRepository;
import ro.ionutscheianu.utils.Utils;

import java.io.IOException;
import java.util.Properties;

public class Server {
    private io.grpc.Server server;

    public Server() throws IOException {
        Properties properties = Utils.loadProperties();
        server = ServerBuilder.
                forPort(Integer.parseInt(properties.getProperty("port")))
                .addService(new ZodiacRepository())
                .addService(new WinterService())
                .addService(new SpringService())
                .addService(new SummerService())
                .addService(new AutumnService())
                .build();
    }

    public void start(){
        System.out.println("Starting server...");
        try {
            server.start();
            System.out.println("Server started, port: " + server.getPort());
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
