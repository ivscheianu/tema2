package ro.ionutscheianu.server;

import ro.ionutscheianu.utils.Utils;
import ro.ionutscheianu.utils.ZodiacSignsLoader;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

public class SignAnalyzer {

    private final Map<String, Interval> signs;

    public SignAnalyzer() throws IOException {
        signs = ZodiacSignsLoader.getZodiacSigns();
    }

    public String determineSign(String birthDate) {
        Date date = Utils.getCustomDateFromString(birthDate);
        if (isAquarius(date)) {
            return "Aquarius";
        }
        if (isAries(date)) {
            return "Aries";
        }
        if (isTaurus(date)) {
            return "Taurus";
        }
        if (isGemini(date)) {
            return "Gemini";
        }
        if (isCancer(date)) {
            return "Cancer";
        }
        if (isLeo(date)) {
            return "Leo";
        }
        if (isVirgo(date)) {
            return "Virgo";
        }
        if (isLibra(date)) {
            return "Libra";
        }
        if (isScorpio(date)) {
            return "Scorpio";
        }
        if (isSagittarius(date)) {
            return "Sagittarius";
        }
        if(isPisces(date)){
            return "Pisces";
        }
        return "Capricorn";
    }

    private boolean isAquarius(Date date) {
        return signs.get("Aquarius").contains(date);
    }

    private boolean isPisces(Date date) {
        return signs.get("Pisces").contains(date);
    }

    private boolean isAries(Date date) {
        return signs.get("Aries").contains(date);
    }

    private boolean isTaurus(Date date) {
        return signs.get("Taurus").contains(date);
    }

    private boolean isGemini(Date date) {
        return signs.get("Gemini").contains(date);
    }

    private boolean isCancer(Date date) {
        return signs.get("Cancer").contains(date);
    }

    private boolean isLeo(Date date) {
        return signs.get("Leo").contains(date);
    }

    private boolean isVirgo(Date date) {
        return signs.get("Virgo").contains(date);
    }

    private boolean isLibra(Date date) {
        return signs.get("Libra").contains(date);
    }

    private boolean isScorpio(Date date) {
        return signs.get("Scorpio").contains(date);
    }

    private boolean isSagittarius(Date date) {
        return signs.get("Sagittarius").contains(date);
    }

    private boolean isCapricorn(Date date) {
        return signs.get("Capricorn").contains(date);
    }
}
