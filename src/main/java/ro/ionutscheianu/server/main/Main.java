package ro.ionutscheianu.server.main;

import ro.ionutscheianu.server.Server;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.start();
    }
}
