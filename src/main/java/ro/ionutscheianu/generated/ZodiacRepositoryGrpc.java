package ro.ionutscheianu.generated;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: ZodiacRepository.proto")
public final class ZodiacRepositoryGrpc {

  private ZodiacRepositoryGrpc() {}

  public static final String SERVICE_NAME = "ZodiacRepository";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest,
      ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse> getDetermineServiceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "DetermineService",
      requestType = ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest.class,
      responseType = ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest,
      ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse> getDetermineServiceMethod() {
    io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest, ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse> getDetermineServiceMethod;
    if ((getDetermineServiceMethod = ZodiacRepositoryGrpc.getDetermineServiceMethod) == null) {
      synchronized (ZodiacRepositoryGrpc.class) {
        if ((getDetermineServiceMethod = ZodiacRepositoryGrpc.getDetermineServiceMethod) == null) {
          ZodiacRepositoryGrpc.getDetermineServiceMethod = getDetermineServiceMethod = 
              io.grpc.MethodDescriptor.<ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest, ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "ZodiacRepository", "DetermineService"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ZodiacRepositoryMethodDescriptorSupplier("DetermineService"))
                  .build();
          }
        }
     }
     return getDetermineServiceMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ZodiacRepositoryStub newStub(io.grpc.Channel channel) {
    return new ZodiacRepositoryStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ZodiacRepositoryBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ZodiacRepositoryBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ZodiacRepositoryFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ZodiacRepositoryFutureStub(channel);
  }

  /**
   */
  public static abstract class ZodiacRepositoryImplBase implements io.grpc.BindableService {

    /**
     */
    public void determineService(ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest request,
        io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDetermineServiceMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getDetermineServiceMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest,
                ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse>(
                  this, METHODID_DETERMINE_SERVICE)))
          .build();
    }
  }

  /**
   */
  public static final class ZodiacRepositoryStub extends io.grpc.stub.AbstractStub<ZodiacRepositoryStub> {
    private ZodiacRepositoryStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacRepositoryStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacRepositoryStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacRepositoryStub(channel, callOptions);
    }

    /**
     */
    public void determineService(ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest request,
        io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDetermineServiceMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ZodiacRepositoryBlockingStub extends io.grpc.stub.AbstractStub<ZodiacRepositoryBlockingStub> {
    private ZodiacRepositoryBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacRepositoryBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacRepositoryBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacRepositoryBlockingStub(channel, callOptions);
    }

    /**
     */
    public ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse determineService(ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest request) {
      return blockingUnaryCall(
          getChannel(), getDetermineServiceMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ZodiacRepositoryFutureStub extends io.grpc.stub.AbstractStub<ZodiacRepositoryFutureStub> {
    private ZodiacRepositoryFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacRepositoryFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacRepositoryFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacRepositoryFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse> determineService(
        ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDetermineServiceMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_DETERMINE_SERVICE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ZodiacRepositoryImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ZodiacRepositoryImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DETERMINE_SERVICE:
          serviceImpl.determineService((ro.ionutscheianu.generated.ServiceRequestOuterClass.ServiceRequest) request,
              (io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ServiceResponseOuterClass.ServiceResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ZodiacRepositoryBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ZodiacRepositoryBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ro.ionutscheianu.generated.ZodiacRepositoryOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ZodiacRepository");
    }
  }

  private static final class ZodiacRepositoryFileDescriptorSupplier
      extends ZodiacRepositoryBaseDescriptorSupplier {
    ZodiacRepositoryFileDescriptorSupplier() {}
  }

  private static final class ZodiacRepositoryMethodDescriptorSupplier
      extends ZodiacRepositoryBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ZodiacRepositoryMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ZodiacRepositoryGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ZodiacRepositoryFileDescriptorSupplier())
              .addMethod(getDetermineServiceMethod())
              .build();
        }
      }
    }
    return result;
  }
}
