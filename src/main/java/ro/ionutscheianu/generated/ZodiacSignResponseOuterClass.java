// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ZodiacSignResponse.proto

package ro.ionutscheianu.generated;

public final class ZodiacSignResponseOuterClass {
  private ZodiacSignResponseOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface ZodiacSignResponseOrBuilder extends
      // @@protoc_insertion_point(interface_extends:ZodiacSignResponse)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>string zodiacSign = 1;</code>
     */
    java.lang.String getZodiacSign();
    /**
     * <code>string zodiacSign = 1;</code>
     */
    com.google.protobuf.ByteString
        getZodiacSignBytes();
  }
  /**
   * Protobuf type {@code ZodiacSignResponse}
   */
  public  static final class ZodiacSignResponse extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:ZodiacSignResponse)
      ZodiacSignResponseOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use ZodiacSignResponse.newBuilder() to construct.
    private ZodiacSignResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private ZodiacSignResponse() {
      zodiacSign_ = "";
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private ZodiacSignResponse(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              java.lang.String s = input.readStringRequireUtf8();

              zodiacSign_ = s;
              break;
            }
            default: {
              if (!parseUnknownFieldProto3(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.internal_static_ZodiacSignResponse_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.internal_static_ZodiacSignResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.class, ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.Builder.class);
    }

    public static final int ZODIACSIGN_FIELD_NUMBER = 1;
    private volatile java.lang.Object zodiacSign_;
    /**
     * <code>string zodiacSign = 1;</code>
     */
    public java.lang.String getZodiacSign() {
      java.lang.Object ref = zodiacSign_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        zodiacSign_ = s;
        return s;
      }
    }
    /**
     * <code>string zodiacSign = 1;</code>
     */
    public com.google.protobuf.ByteString
        getZodiacSignBytes() {
      java.lang.Object ref = zodiacSign_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        zodiacSign_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private byte memoizedIsInitialized = -1;
    @java.lang.Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (!getZodiacSignBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 1, zodiacSign_);
      }
      unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (!getZodiacSignBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, zodiacSign_);
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse)) {
        return super.equals(obj);
      }
      ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse other = (ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse) obj;

      boolean result = true;
      result = result && getZodiacSign()
          .equals(other.getZodiacSign());
      result = result && unknownFields.equals(other.unknownFields);
      return result;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + ZODIACSIGN_FIELD_NUMBER;
      hash = (53 * hash) + getZodiacSign().hashCode();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @java.lang.Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @java.lang.Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code ZodiacSignResponse}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:ZodiacSignResponse)
        ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponseOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.internal_static_ZodiacSignResponse_descriptor;
      }

      @java.lang.Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.internal_static_ZodiacSignResponse_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.class, ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.Builder.class);
      }

      // Construct using ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @java.lang.Override
      public Builder clear() {
        super.clear();
        zodiacSign_ = "";

        return this;
      }

      @java.lang.Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.internal_static_ZodiacSignResponse_descriptor;
      }

      @java.lang.Override
      public ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse getDefaultInstanceForType() {
        return ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.getDefaultInstance();
      }

      @java.lang.Override
      public ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse build() {
        ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @java.lang.Override
      public ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse buildPartial() {
        ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse result = new ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse(this);
        result.zodiacSign_ = zodiacSign_;
        onBuilt();
        return result;
      }

      @java.lang.Override
      public Builder clone() {
        return (Builder) super.clone();
      }
      @java.lang.Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return (Builder) super.setField(field, value);
      }
      @java.lang.Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }
      @java.lang.Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }
      @java.lang.Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, java.lang.Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }
      @java.lang.Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }
      @java.lang.Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse) {
          return mergeFrom((ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse other) {
        if (other == ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.getDefaultInstance()) return this;
        if (!other.getZodiacSign().isEmpty()) {
          zodiacSign_ = other.zodiacSign_;
          onChanged();
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @java.lang.Override
      public final boolean isInitialized() {
        return true;
      }

      @java.lang.Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private java.lang.Object zodiacSign_ = "";
      /**
       * <code>string zodiacSign = 1;</code>
       */
      public java.lang.String getZodiacSign() {
        java.lang.Object ref = zodiacSign_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          zodiacSign_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>string zodiacSign = 1;</code>
       */
      public com.google.protobuf.ByteString
          getZodiacSignBytes() {
        java.lang.Object ref = zodiacSign_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          zodiacSign_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>string zodiacSign = 1;</code>
       */
      public Builder setZodiacSign(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        zodiacSign_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>string zodiacSign = 1;</code>
       */
      public Builder clearZodiacSign() {
        
        zodiacSign_ = getDefaultInstance().getZodiacSign();
        onChanged();
        return this;
      }
      /**
       * <code>string zodiacSign = 1;</code>
       */
      public Builder setZodiacSignBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        zodiacSign_ = value;
        onChanged();
        return this;
      }
      @java.lang.Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFieldsProto3(unknownFields);
      }

      @java.lang.Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:ZodiacSignResponse)
    }

    // @@protoc_insertion_point(class_scope:ZodiacSignResponse)
    private static final ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse();
    }

    public static ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<ZodiacSignResponse>
        PARSER = new com.google.protobuf.AbstractParser<ZodiacSignResponse>() {
      @java.lang.Override
      public ZodiacSignResponse parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new ZodiacSignResponse(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<ZodiacSignResponse> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<ZodiacSignResponse> getParserForType() {
      return PARSER;
    }

    @java.lang.Override
    public ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ZodiacSignResponse_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ZodiacSignResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\030ZodiacSignResponse.proto\"(\n\022ZodiacSign" +
      "Response\022\022\n\nzodiacSign\030\001 \001(\tB\034\n\032ro.ionut" +
      "scheianu.generatedb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_ZodiacSignResponse_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_ZodiacSignResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ZodiacSignResponse_descriptor,
        new java.lang.String[] { "ZodiacSign", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
