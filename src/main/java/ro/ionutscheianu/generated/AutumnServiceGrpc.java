package ro.ionutscheianu.generated;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: AutumnService.proto")
public final class AutumnServiceGrpc {

  private AutumnServiceGrpc() {}

  public static final String SERVICE_NAME = "AutumnService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest,
      ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> getDetermineSignMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "DetermineSign",
      requestType = ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.class,
      responseType = ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest,
      ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> getDetermineSignMethod() {
    io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest, ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> getDetermineSignMethod;
    if ((getDetermineSignMethod = AutumnServiceGrpc.getDetermineSignMethod) == null) {
      synchronized (AutumnServiceGrpc.class) {
        if ((getDetermineSignMethod = AutumnServiceGrpc.getDetermineSignMethod) == null) {
          AutumnServiceGrpc.getDetermineSignMethod = getDetermineSignMethod = 
              io.grpc.MethodDescriptor.<ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest, ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "AutumnService", "DetermineSign"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new AutumnServiceMethodDescriptorSupplier("DetermineSign"))
                  .build();
          }
        }
     }
     return getDetermineSignMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AutumnServiceStub newStub(io.grpc.Channel channel) {
    return new AutumnServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AutumnServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new AutumnServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AutumnServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new AutumnServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class AutumnServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void determineSign(ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest request,
        io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDetermineSignMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getDetermineSignMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest,
                ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse>(
                  this, METHODID_DETERMINE_SIGN)))
          .build();
    }
  }

  /**
   */
  public static final class AutumnServiceStub extends io.grpc.stub.AbstractStub<AutumnServiceStub> {
    private AutumnServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AutumnServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AutumnServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AutumnServiceStub(channel, callOptions);
    }

    /**
     */
    public void determineSign(ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest request,
        io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDetermineSignMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class AutumnServiceBlockingStub extends io.grpc.stub.AbstractStub<AutumnServiceBlockingStub> {
    private AutumnServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AutumnServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AutumnServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AutumnServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse determineSign(ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest request) {
      return blockingUnaryCall(
          getChannel(), getDetermineSignMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class AutumnServiceFutureStub extends io.grpc.stub.AbstractStub<AutumnServiceFutureStub> {
    private AutumnServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AutumnServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AutumnServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AutumnServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> determineSign(
        ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDetermineSignMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_DETERMINE_SIGN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AutumnServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AutumnServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DETERMINE_SIGN:
          serviceImpl.determineSign((ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest) request,
              (io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class AutumnServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    AutumnServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ro.ionutscheianu.generated.AutumnServiceOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("AutumnService");
    }
  }

  private static final class AutumnServiceFileDescriptorSupplier
      extends AutumnServiceBaseDescriptorSupplier {
    AutumnServiceFileDescriptorSupplier() {}
  }

  private static final class AutumnServiceMethodDescriptorSupplier
      extends AutumnServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    AutumnServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AutumnServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new AutumnServiceFileDescriptorSupplier())
              .addMethod(getDetermineSignMethod())
              .build();
        }
      }
    }
    return result;
  }
}
