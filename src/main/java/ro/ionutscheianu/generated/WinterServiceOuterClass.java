// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: WinterService.proto

package ro.ionutscheianu.generated;

public final class WinterServiceOuterClass {
  private WinterServiceOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\023WinterService.proto\032\027ZodiacSignRequest" +
      ".proto\032\030ZodiacSignResponse.proto2I\n\rWint" +
      "erService\0228\n\rDetermineSign\022\022.ZodiacSignR" +
      "equest\032\023.ZodiacSignResponseB\034\n\032ro.ionuts" +
      "cheianu.generatedb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.getDescriptor(),
          ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.getDescriptor(),
        }, assigner);
    ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.getDescriptor();
    ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
