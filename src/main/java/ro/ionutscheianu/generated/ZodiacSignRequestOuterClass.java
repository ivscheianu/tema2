// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ZodiacSignRequest.proto

package ro.ionutscheianu.generated;

public final class ZodiacSignRequestOuterClass {
  private ZodiacSignRequestOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface ZodiacSignRequestOrBuilder extends
      // @@protoc_insertion_point(interface_extends:ZodiacSignRequest)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>string birthDate = 1;</code>
     */
    java.lang.String getBirthDate();
    /**
     * <code>string birthDate = 1;</code>
     */
    com.google.protobuf.ByteString
        getBirthDateBytes();
  }
  /**
   * Protobuf type {@code ZodiacSignRequest}
   */
  public  static final class ZodiacSignRequest extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:ZodiacSignRequest)
      ZodiacSignRequestOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use ZodiacSignRequest.newBuilder() to construct.
    private ZodiacSignRequest(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private ZodiacSignRequest() {
      birthDate_ = "";
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private ZodiacSignRequest(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              java.lang.String s = input.readStringRequireUtf8();

              birthDate_ = s;
              break;
            }
            default: {
              if (!parseUnknownFieldProto3(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.internal_static_ZodiacSignRequest_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.internal_static_ZodiacSignRequest_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.class, ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.Builder.class);
    }

    public static final int BIRTHDATE_FIELD_NUMBER = 1;
    private volatile java.lang.Object birthDate_;
    /**
     * <code>string birthDate = 1;</code>
     */
    public java.lang.String getBirthDate() {
      java.lang.Object ref = birthDate_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        birthDate_ = s;
        return s;
      }
    }
    /**
     * <code>string birthDate = 1;</code>
     */
    public com.google.protobuf.ByteString
        getBirthDateBytes() {
      java.lang.Object ref = birthDate_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        birthDate_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private byte memoizedIsInitialized = -1;
    @java.lang.Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (!getBirthDateBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 1, birthDate_);
      }
      unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (!getBirthDateBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, birthDate_);
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest)) {
        return super.equals(obj);
      }
      ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest other = (ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest) obj;

      boolean result = true;
      result = result && getBirthDate()
          .equals(other.getBirthDate());
      result = result && unknownFields.equals(other.unknownFields);
      return result;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + BIRTHDATE_FIELD_NUMBER;
      hash = (53 * hash) + getBirthDate().hashCode();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @java.lang.Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @java.lang.Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code ZodiacSignRequest}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:ZodiacSignRequest)
        ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequestOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.internal_static_ZodiacSignRequest_descriptor;
      }

      @java.lang.Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.internal_static_ZodiacSignRequest_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.class, ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.Builder.class);
      }

      // Construct using ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @java.lang.Override
      public Builder clear() {
        super.clear();
        birthDate_ = "";

        return this;
      }

      @java.lang.Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.internal_static_ZodiacSignRequest_descriptor;
      }

      @java.lang.Override
      public ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest getDefaultInstanceForType() {
        return ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.getDefaultInstance();
      }

      @java.lang.Override
      public ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest build() {
        ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @java.lang.Override
      public ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest buildPartial() {
        ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest result = new ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest(this);
        result.birthDate_ = birthDate_;
        onBuilt();
        return result;
      }

      @java.lang.Override
      public Builder clone() {
        return (Builder) super.clone();
      }
      @java.lang.Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return (Builder) super.setField(field, value);
      }
      @java.lang.Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }
      @java.lang.Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }
      @java.lang.Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, java.lang.Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }
      @java.lang.Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }
      @java.lang.Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest) {
          return mergeFrom((ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest other) {
        if (other == ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.getDefaultInstance()) return this;
        if (!other.getBirthDate().isEmpty()) {
          birthDate_ = other.birthDate_;
          onChanged();
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @java.lang.Override
      public final boolean isInitialized() {
        return true;
      }

      @java.lang.Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private java.lang.Object birthDate_ = "";
      /**
       * <code>string birthDate = 1;</code>
       */
      public java.lang.String getBirthDate() {
        java.lang.Object ref = birthDate_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          birthDate_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>string birthDate = 1;</code>
       */
      public com.google.protobuf.ByteString
          getBirthDateBytes() {
        java.lang.Object ref = birthDate_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          birthDate_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>string birthDate = 1;</code>
       */
      public Builder setBirthDate(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        birthDate_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>string birthDate = 1;</code>
       */
      public Builder clearBirthDate() {
        
        birthDate_ = getDefaultInstance().getBirthDate();
        onChanged();
        return this;
      }
      /**
       * <code>string birthDate = 1;</code>
       */
      public Builder setBirthDateBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        birthDate_ = value;
        onChanged();
        return this;
      }
      @java.lang.Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFieldsProto3(unknownFields);
      }

      @java.lang.Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:ZodiacSignRequest)
    }

    // @@protoc_insertion_point(class_scope:ZodiacSignRequest)
    private static final ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest();
    }

    public static ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<ZodiacSignRequest>
        PARSER = new com.google.protobuf.AbstractParser<ZodiacSignRequest>() {
      @java.lang.Override
      public ZodiacSignRequest parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new ZodiacSignRequest(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<ZodiacSignRequest> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<ZodiacSignRequest> getParserForType() {
      return PARSER;
    }

    @java.lang.Override
    public ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ZodiacSignRequest_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ZodiacSignRequest_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\027ZodiacSignRequest.proto\"&\n\021ZodiacSignR" +
      "equest\022\021\n\tbirthDate\030\001 \001(\tB\034\n\032ro.ionutsch" +
      "eianu.generatedb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_ZodiacSignRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_ZodiacSignRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ZodiacSignRequest_descriptor,
        new java.lang.String[] { "BirthDate", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
