package ro.ionutscheianu.generated;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: SummerService.proto")
public final class SummerServiceGrpc {

  private SummerServiceGrpc() {}

  public static final String SERVICE_NAME = "SummerService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest,
      ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> getDetermineSignMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "DetermineSign",
      requestType = ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.class,
      responseType = ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest,
      ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> getDetermineSignMethod() {
    io.grpc.MethodDescriptor<ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest, ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> getDetermineSignMethod;
    if ((getDetermineSignMethod = SummerServiceGrpc.getDetermineSignMethod) == null) {
      synchronized (SummerServiceGrpc.class) {
        if ((getDetermineSignMethod = SummerServiceGrpc.getDetermineSignMethod) == null) {
          SummerServiceGrpc.getDetermineSignMethod = getDetermineSignMethod = 
              io.grpc.MethodDescriptor.<ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest, ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "SummerService", "DetermineSign"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new SummerServiceMethodDescriptorSupplier("DetermineSign"))
                  .build();
          }
        }
     }
     return getDetermineSignMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SummerServiceStub newStub(io.grpc.Channel channel) {
    return new SummerServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SummerServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new SummerServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SummerServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new SummerServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class SummerServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void determineSign(ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest request,
        io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDetermineSignMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getDetermineSignMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest,
                ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse>(
                  this, METHODID_DETERMINE_SIGN)))
          .build();
    }
  }

  /**
   */
  public static final class SummerServiceStub extends io.grpc.stub.AbstractStub<SummerServiceStub> {
    private SummerServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SummerServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SummerServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SummerServiceStub(channel, callOptions);
    }

    /**
     */
    public void determineSign(ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest request,
        io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDetermineSignMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class SummerServiceBlockingStub extends io.grpc.stub.AbstractStub<SummerServiceBlockingStub> {
    private SummerServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SummerServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SummerServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SummerServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse determineSign(ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest request) {
      return blockingUnaryCall(
          getChannel(), getDetermineSignMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SummerServiceFutureStub extends io.grpc.stub.AbstractStub<SummerServiceFutureStub> {
    private SummerServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SummerServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SummerServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SummerServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse> determineSign(
        ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDetermineSignMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_DETERMINE_SIGN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SummerServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SummerServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DETERMINE_SIGN:
          serviceImpl.determineSign((ro.ionutscheianu.generated.ZodiacSignRequestOuterClass.ZodiacSignRequest) request,
              (io.grpc.stub.StreamObserver<ro.ionutscheianu.generated.ZodiacSignResponseOuterClass.ZodiacSignResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SummerServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SummerServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ro.ionutscheianu.generated.SummerServiceOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SummerService");
    }
  }

  private static final class SummerServiceFileDescriptorSupplier
      extends SummerServiceBaseDescriptorSupplier {
    SummerServiceFileDescriptorSupplier() {}
  }

  private static final class SummerServiceMethodDescriptorSupplier
      extends SummerServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SummerServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SummerServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SummerServiceFileDescriptorSupplier())
              .addMethod(getDetermineSignMethod())
              .build();
        }
      }
    }
    return result;
  }
}
