package ro.ionutscheianu.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import ro.ionutscheianu.generated.AutumnServiceGrpc;
import ro.ionutscheianu.generated.ServiceRequestOuterClass;
import ro.ionutscheianu.generated.ServiceResponseOuterClass;
import ro.ionutscheianu.generated.SpringServiceGrpc;
import ro.ionutscheianu.generated.SummerServiceGrpc;
import ro.ionutscheianu.generated.WinterServiceGrpc;
import ro.ionutscheianu.generated.ZodiacRepositoryGrpc;
import ro.ionutscheianu.generated.ZodiacSignRequestOuterClass;
import ro.ionutscheianu.utils.Utils;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Properties;
import java.util.Scanner;

public class Client {
    private ManagedChannel managedChannel;

    public Client() {
        Properties properties = Utils.loadProperties();
        managedChannel = ManagedChannelBuilder.forAddress(properties.getProperty("ip"), Integer.parseInt(properties.getProperty("port"))).usePlaintext().build();
    }

    public void start() {
        String input;
        System.out.println("Write 'exit!' to stop the program.");
        do {
            input = getUserInput();
            try {
                LocalDate date = Utils.getLocalDateFromString(input);
                ServiceResponseOuterClass.ServiceType serviceType = getAppropriateService(input);
                callAppropriateService(input, serviceType);
            } catch (DateTimeParseException dateTimeParseException) {
                System.out.println("Invalid date");
            }
        } while (!input.equalsIgnoreCase("exit!"));
        System.out.println("Exit called.");
    }

    private String getUserInput() {
        String input;
        System.out.println("Insert date(MM/dd/yyyy):");
        Scanner scanner = new Scanner(System.in);
        input = scanner.nextLine();
        return input;
    }

    private void callAppropriateService(String input, ServiceResponseOuterClass.ServiceType serviceType) {
        switch (serviceType) {
            case WINTER:
                System.out.println(getSignFromWinter(input));
                break;
            case SPRING:
                System.out.println(getSignFromSpring(input));
                break;
            case SUMMER:
                System.out.println(getSignFromSummer(input));
                break;
            case AUTUMN:
                System.out.println(getSignFromAutumn(input));
                break;
            case UNRECOGNIZED:
                System.out.println("Unknown date");
                break;
        }
    }

    private ServiceResponseOuterClass.ServiceType getAppropriateService(String birthDate) {
        ZodiacRepositoryGrpc.ZodiacRepositoryBlockingStub zodiacRepositoryBlockingStub = ZodiacRepositoryGrpc.newBlockingStub(managedChannel);
        ServiceRequestOuterClass.ServiceRequest serviceRequest = ServiceRequestOuterClass.ServiceRequest
                .newBuilder()
                .setBirthDate(birthDate)
                .build();
        return zodiacRepositoryBlockingStub.determineService(serviceRequest).getService();
    }

    private String getSignFromWinter(String birthDate) {
        WinterServiceGrpc.WinterServiceBlockingStub winterServiceBlockingStub = WinterServiceGrpc.newBlockingStub(managedChannel);
        ZodiacSignRequestOuterClass.ZodiacSignRequest zodiacSignRequest = ZodiacSignRequestOuterClass.ZodiacSignRequest
                .newBuilder()
                .setBirthDate(birthDate)
                .build();
        return winterServiceBlockingStub.determineSign(zodiacSignRequest).getZodiacSign();
    }

    private String getSignFromSpring(String birthDate) {
        SpringServiceGrpc.SpringServiceBlockingStub SpringServiceBlockingStub = SpringServiceGrpc.newBlockingStub(managedChannel);
        ZodiacSignRequestOuterClass.ZodiacSignRequest zodiacSignRequest = ZodiacSignRequestOuterClass.ZodiacSignRequest
                .newBuilder()
                .setBirthDate(birthDate)
                .build();
        return SpringServiceBlockingStub.determineSign(zodiacSignRequest).getZodiacSign();
    }

    private String getSignFromSummer(String birthDate) {
        SummerServiceGrpc.SummerServiceBlockingStub SummerServiceBlockingStub = SummerServiceGrpc.newBlockingStub(managedChannel);
        ZodiacSignRequestOuterClass.ZodiacSignRequest zodiacSignRequest = ZodiacSignRequestOuterClass.ZodiacSignRequest
                .newBuilder()
                .setBirthDate(birthDate)
                .build();
        return SummerServiceBlockingStub.determineSign(zodiacSignRequest).getZodiacSign();
    }

    private String getSignFromAutumn(String birthDate) {
        AutumnServiceGrpc.AutumnServiceBlockingStub AutumnServiceBlockingStub = AutumnServiceGrpc.newBlockingStub(managedChannel);
        ZodiacSignRequestOuterClass.ZodiacSignRequest zodiacSignRequest = ZodiacSignRequestOuterClass.ZodiacSignRequest
                .newBuilder()
                .setBirthDate(birthDate)
                .build();
        return AutumnServiceBlockingStub.determineSign(zodiacSignRequest).getZodiacSign();
    }

}
