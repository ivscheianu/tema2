package ro.ionutscheianu.utils;

import ro.ionutscheianu.server.Interval;
import ro.ionutscheianu.server.Pair;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public final class ZodiacSignsLoader {

    private ZodiacSignsLoader(){

    }

    public static Map<String, Interval> getZodiacSigns() throws IOException {
        String[] lines = ReadFromFile.readLinesFromTextFile("src/main/resources/interval/intervals.txt");
        Map<String, Interval> hashMap = new HashMap<>();
        Stream.iterate(0,index->++index)
                .limit(lines.length)
                .forEach(index->{
                    Pair<String, Interval> entry = getEntryFromLine(lines[index]);
                    hashMap.put(entry.getFirst(), entry.getSecond());
                });
        return hashMap;
    }

    private static Pair<String, Interval> getEntryFromLine(String line){
        String[] split = line.split(" ");
        String zodiacSignName = split[0];
        Interval interval = getIntervalFromString(split[1]);
        return new Pair<>(zodiacSignName, interval);
    }

    private static Interval getIntervalFromString(String string){
        String[] split = string.split("-");
        String startingDateString = split[0];
        String endingDateString = split[1];
        int month = Integer.parseInt(startingDateString.substring(0,2)) - 1;
        int day = Integer.parseInt(startingDateString.substring(3,5));
        Date startingDate = Utils.getDateFromInt(month, day);
        month = Integer.parseInt(endingDateString.substring(0,2)) - 1;
        day = Integer.parseInt(endingDateString.substring(3,5));
        Date endingDate =  Utils.getDateFromInt(month, day);
        return new Interval(startingDate, endingDate);
    }
}
