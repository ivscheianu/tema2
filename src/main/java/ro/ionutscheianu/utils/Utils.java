package ro.ionutscheianu.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public final class Utils {

    private Utils() {

    }

    public static Properties loadProperties() {
        Properties properties = null;
        try (InputStream input = new FileInputStream("src/main/resources/properties/config.properties")) {
            properties = new Properties();
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (properties == null) {
            throw new NullPointerException();
        }
        return properties;
    }

    public static LocalDate getLocalDateFromString(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
        return LocalDate.parse(date, formatter);
    }

    public static Date getCustomDateFromString(String birthDate){
        LocalDate localDate = Utils.getLocalDateFromString(birthDate);
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.MONTH, localDate.getMonthValue() - 1);
        calendar.set(Calendar.DAY_OF_MONTH, localDate.getDayOfMonth());
        return calendar.getTime();
    }

    public static Date getDateFromInt(int month, int day){
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

}
